package com.streams.eda.samples.order.monitor;

import com.streams.samples.*;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.util.Collections.singletonMap;

@Slf4j
@EnableBinding(OrderMonitoringProcessor.StreamProcessor.class)
public class OrderMonitoringProcessor {
    @Value("${spring.cloud.stream.kafka.streams.binder.configuration.schema.registry.url}")
    private String schemaRegistryUrl;
    private Map<String, Timer> workflowStepTimers = new HashMap<>();
    private Map<String, Counter> workflowStepTotalCounter = new HashMap<>();
    private Counter placedOrderCounter;
    private Counter expiredOrderCounter;
    private Counter completedOrderCounter;

    public OrderMonitoringProcessor(MeterRegistry registry) {
        this.placedOrderCounter = registry.counter("PlacedOrders");
        this.expiredOrderCounter = registry.counter("ExpiredOrders");
        this.completedOrderCounter = registry.counter("CompletedOrders");

        workflowStepTimers.put("order-placed", registry.timer("OrderPlacedStepDurationTimer"));
        workflowStepTimers.put("order-stock-reserved", registry.timer("OrderStockReservationStepDurationTimer"));
        workflowStepTimers.put("order-shipped", registry.timer("OrderShippingDurationTimer"));
        workflowStepTimers.put("order-invoiced", registry.timer("OrderPaymentDurationTimer"));
        workflowStepTimers.put("order-completed", registry.timer("EndToEndOrderProcessingDuration"));

        workflowStepTotalCounter.put("order-placed", registry.counter("TotalPlacedOrders"));
        workflowStepTotalCounter.put("order-stock-reserved", registry.counter("TotalOrderStockReservations"));
        workflowStepTotalCounter.put("order-shipped", registry.counter("TotalShippedOrders"));
        workflowStepTotalCounter.put("order-invoiced", registry.counter("TotalInvoicedOrders"));
        workflowStepTotalCounter.put("order-completed", registry.counter("TotalCompletedOrders"));
    }

    @StreamListener
    public void process(@Input("orderPlaced") KStream<OrderKey, OrderPlaced> orderPlacedStream,
                        @Input("orderExpired") KStream<OrderKey, OrderExpired> orderExpiredStream,
                        @Input("orderCompleted") KStream<OrderKey, OrderCompleted> orderCompletedStream,
                        @Input("orderEndToEndFlowStream") KStream<OrderKey, OrderEndToEndFlowSteps> orderEndToEndFlowStream,
                        @Input("orderWorkflowStepCompleted") KStream<OrderKey, OrderWorkflowStepCompleted> orderWorkflowStepCompletedStream) {
        orderPlacedStream.peek((k, v) -> placedOrderCounter.increment());
        orderExpiredStream.peek((k, v) -> expiredOrderCounter.increment());
        orderCompletedStream.peek((k, v) -> completedOrderCounter.increment());

        trackTotalOrderWorkflowSteps(orderWorkflowStepCompletedStream);
        trackWorkflowStepProcessingDuration(orderEndToEndFlowStream, orderWorkflowStepCompletedStream);
    }

    private void trackTotalOrderWorkflowSteps(KStream<OrderKey, OrderWorkflowStepCompleted> orderWorkflowStepCompletedStream) {
        orderWorkflowStepCompletedStream.peek((k, v) -> workflowStepTotalCounter.get(v.getStepName()).increment());
    }

    private void trackWorkflowStepProcessingDuration(KStream<OrderKey, OrderEndToEndFlowSteps> orderEndToEndFlowStream,
                                                     KStream<OrderKey, OrderWorkflowStepCompleted> orderWorkflowStepCompletedStream) {
        //A workaround for issue https://stackoverflow.com/questions/56548736/kstream-kstream-inner-join-throws-java-lang-classcastexception
        orderEndToEndFlowStream = orderEndToEndFlowStream.mapValues(v -> v);
        orderWorkflowStepCompletedStream = orderWorkflowStepCompletedStream.mapValues(v -> v);

        var joinWindow = JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());

        var joinedOrderWorkflowStream = orderEndToEndFlowStream.join(orderWorkflowStepCompletedStream
                , Tuple::of, joinWindow,
                Joined.with(avroSerde(true), avroSerde(false), avroSerde(false)));

        joinedOrderWorkflowStream.peek(this::onOrderEndToEndFlowRecord);
    }

    private void onOrderEndToEndFlowRecord(OrderKey orderKey, Tuple2<OrderEndToEndFlowSteps, OrderWorkflowStepCompleted> value) {
        var currentWorkflowStep = value._2;

        var workflowStepCompletedTime = Stream.range(0, value._1.getWORKFLOWSTEPS().size())
                .map(i -> Tuple.of(value._1.getWORKFLOWSTEPS().get(i), value._1.getWORKFLOWSTEPCOMPLETIONTIMES().get(i)))
                .collect(LinkedHashMap.collector());

        var previousStepCompletedTime = workflowStepCompletedTime.get(value._2.getPreviousWorkflowStep());

        previousStepCompletedTime
                .peek(v -> workflowStepTimers.get(currentWorkflowStep.getStepName())
                        .record(currentWorkflowStep.getCompletionTime() - v, TimeUnit.MILLISECONDS));
    }

    public <T extends SpecificRecord> Serde<T> avroSerde(boolean isKey) {
        var serdeConfig = singletonMap("schema.registry.url", schemaRegistryUrl);
        Serde<T> serde = new SpecificAvroSerde<>();
        serde.configure(serdeConfig, isKey);
        return serde;
    }

    public interface StreamProcessor {
        @Input("orderPlaced")
        KStream<?, ?> orderPlaced();

        @Input("orderExpired")
        KStream<?, ?> orderExpired();

        @Input("orderCompleted")
        KStream<?, ?> orderCompleted();

        @Input("orderEndToEndFlowStream")
        KStream<?, ?> orderEndToEndFlowStream();

        @Input("orderWorkflowStepCompleted")
        KStream<?, ?> orderWorkflowStepCompleted();
    }
}
