package com.streams.eda.samples.order;

import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderServiceAppConfig {
    @Value("${spring.cloud.stream.kafka.streams.binder.configuration.schema.registry.url}")
    private String endPoint;

    @Bean
    public SchemaRegistryClient schemaRegistryClient() {
        ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
        client.setEndpoint(endPoint);
        return client;
    }
}
