# Order monitoring service

A monitoring service for the order business process. This service uses 
`spring-metrics` to export order metrics to `Prometheus`.

## Exported metrics

### Placed orders

It consumes `order-placed` event stream to export a counter of all placed orders.

### Expired orders

A counter of all orders that are expired. This is calculated from consuming 
the `order-expired` event stream.

### Completed orders

A counter of all orders that are completed and finished the order 
business process/workflow to the end successfully.

### Order workflow completed steps

A counter for each completed step in the order workflow. The monitoring processor
is consuming the stream of events of type `order-workflow-step-completed`.

### Workflow step processing duration

A timer metrics to track the processing duration for each workflow step.
It consumes event stream of KTable `OrderEndToEndFlowSteps`(this KTable is created
by KSQL as an aggregation of all workflow steps for orders). The processing duration is the period between the previous completed
workflow step to the time of current.